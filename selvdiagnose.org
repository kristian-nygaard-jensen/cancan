* Diagnoser
** Stress
*** Fysisk [40%]
 - [ ] Svimmelhed pga. træthed.
 - [X] Muskelspændinger specielt i nakke og skuldre.
 - [ ] Hovedpine.
 - [-] Tendens til svedeture uden fysisk aktivitet? Måske, sveder en del
 - [X] Åndedrættet bliver hurtigere og mere overfladisk
 - [ ] Hjertebanken med uregelmæssige rytme. Stikkende smerte i hjertet.
 - [ ] Problemer med fordøjelsen: Kvalme, halsbrand, svien i maven og diarré. Eller forstoppelse og appetitløshed: Altid sulten
 - [X] Sexproblemer. Mænd kan blive impotente, og begge køn kan miste lysten til sex.
 - [ ] Nedsat immunforsvar som fx hyppige forkølelse, forsinker sårheling mv.: hoster en del, kunne også være rygning
 - [X] Problemer med at sove.
*** Psykisk [66%]
 - [X] Nedsat hukommelse
 - [X] Irritabilitet
 - [ ] Dårligt humør
 - [ ] Angst
 - [X] Koncentrationsbesvær
 - [X] Ubeslutsomhed
** ADHD
 Kan også være ADD(den stille), er introvert.
*** Ud og hjemme
 Gennemgående tema: Koncentrationsbesvær.
 Stoppe med at lave lektier i 8. klasse, eller holdt det til et absolut minimum.
 +X'en sagde altid at jeg fistrede+. +Pillede ved servietholder(med rulle), kuglepen osv+. Arvet rysteknæ fra min mor.
 Kan ikke sidde stille på arbejde, vandre hvileløst rundt på gangene(når jeg ikke ryger eller henter kaffe), medens tankerne flyver til højre og venstre.
 Får ikke lavet mine 5 ansøgninger om ugen på Jobcenteret.
 Ritalin/Amfetamin har en afslappende effekt. På 3. dagen begynder jeg at se ting, og svede mere end normal.
 Emma giver en var følelse inden i, men det er vist meget normalt.
*** Symptomer
**** koncentration [88%]
  - [X] Laver skødesløse fejl: Er meget nonchalant/lassefar med min penge. Kaster om mig, når jeg har dem.
    Købte en ekstra campingvogn da jeg godt kunne lide teltet(impulsiv handling). Solgte den igen med tab.
  - [X] Kan ikke fastholde opmærksomheden over tid: Lader mig distrahere af radio, artikler på nettet eller noget jeg ser på TV.
    Hvis jeg får en tanke surfer jeg.
  - [X] Hører ikke efter, hvad der bliver sagt: Glemte at lægesekretæren havde sagt, at hun ville passe på Antabussen.
  - [X] Følger ikke instrukser: Går ofte mine egene veje. Følger ikke altid den officielle plan.
  - [X] Kan ikke organisere/planlægge/tilrettelægge aktiviteter: Har prøvet:
    + Emacs org,
    + https://github.com/projecthamster/hamster
    + https://addons.mozilla.org/en-US/firefox/addon/tomato-clock/
    + Atlassian
    + https://www.redmine.org/
    + https://trac.edgewall.org/
    Lige lidt har det hjulpet.
  - [ ] Undgår opgaver som kræver opmærksomhed: Nej siger altid ja. Jeg er en pleaser.
  - [X] Mister til stadighed ting: Adskillige telefoner, nøgler, pas, kørekort, pung, tasker, jakker...
  - [X] Lader sig let distrahere af ydre stimuli: Er det ikke det samme spørgsmål som 2?
  - [X] Er glemsom i forbindelse med dagligdagsaktiviteter: Glemte engang Eva i Peters børnehave, da jeg var på barsel.
**** Hyperaktivitet [80%]
  - [X] Konstant småuro i hænder/fødder: Fistre og rysteknæ(højre)
  - [X] Forlader sin plads: vandre, kaffe, smøger.
  - [ ] Løber, klatrer omkring på utilpasset måde: Bryder mig ikke om at løbe.
  - [X] Har svært ved at være stille: Råber op hvis der er noget som ikke passer mig, kun hvis det er noget jeg ved noget om.
  - [X] Stor motorisk aktivitet, som ikke lader sig styre: Gik på bare tær fra gullandsgade 7 2300 til Copenhagen camping.
    Gik på jagt med luftbøsse og pistol. Overnattende på stranden. Fik besøg af politiet.(Fik god behandling).
**** impulsivitet [50%]
  - [X] Svarer før spørgsmålet er afsluttet: Ved tit hvad folk tænker. Tror det er empati.
  - [ ] Kan ikke vente på det bliver hans/hendes tur: Springer ikke over i køen.
  - [ ] Afbryder eller trænger sig på: Rækker for det meste fingeren op.
  - [X] Snakker for meget uden situationsfornemmelse: Siger det som det er.
** OCD [0%]
 - [ ] Synes du selv, at du vasker dig unødvendigt meget? Nej
 - [ ] Synes du selv, at du skal kontrollere døre, låse, mm. unødvendigt meget? afgjordt ikke.
 - [ ] Har du påtrængende tanker med ubehageligt indhold, som er vanskelige at slippe af med? næ
** PTSD [20%]
 - [ ] Man genoplever den katastrofale hændelse i form af mareridt og påtrængende minder, såkaldte flashbacks. Genoplevelsen præges af angst, søvnløshed og stærke spændinger i musklerne
 - [ ] Stærk trang til at undgå situationer og lignende, som minder om traumet, fx film, TV-programmer og årsdage for traumet.
 - [ ] Generel øget vagtsomhed. Den mindste uventede lyd får én til at fare sammen. ~Det er mere far~
 - [ ] Forandring af personligheden, måske mindsket interesse for ting, som man tidligere var engageret i. Mindsket evne til at føle kærlighed og nærhed til andre mennesker. Mindsket interesse for egen person og ofte ligegyldighed. Dette kan dog også skyldes, at tilstanden er kompliceret af en depression - hvilket sker hyppigt
   I nogen grad
 - [X] Man har besvær med at koncentrere sig og huske
