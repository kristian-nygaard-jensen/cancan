import express from 'express';
import open from 'open';
import jsdom from 'jsdom';
const {JSDOM} = jsdom;
const app = express();
const port = 46047;
app.use(express.static('public'));
app.get('/', (req, res) => {
    const dom = new JSDOM(`<!DOCTYPE html><html lang="da">`);
    const document = dom.window.document;
    const head = document.head;
    const body = document.body;
    document.title = 'CanCan';
    const logo = document.createElement('link');
    logo.setAttribute('rel', 'icon');
    logo.setAttribute('href', '/images/logo.svg');
    logo.setAttribute('type', 'image/svg+xml');
    head.appendChild(logo);
    res.send(dom.serialize());
});

app.post('/Kvicklyt', (req, res) => {
    console.log(req);
});

const server = app.listen(port, async () => {
    console.log(open);
    console.log(`Example app listening on port ${server.address().port}`);
    //await open(`http://127.0.0.1:${server.address().port}`);
    //console.log('Opened');
});
