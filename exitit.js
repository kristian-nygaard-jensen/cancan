import readline from 'node:readline';

const dim = {
    x: 16,
    y: 16,
};

const dig = {
    x: 0,
    y: 0,
};

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

let x;
let y;

let score = 0;
let life = 5;
let timeslot = 60;
let time = timeslot;
let timerInt;

function placeIb(){
    do {
        x = getRandomInt(dim.x);
        y = getRandomInt(dim.y);
    }while(x == dig.x && y == dig.y);
    startTimer();
}
placeIb();

function startTimer(){
    time = timeslot;
    timerInt = setInterval(() => {
        time--;
        if(time == 0){
            decLife();
            clearInterval(timerInt);
            placeIb();
        }
    }, 1000);
}


function decLife(){
    life--;
    if (life == 0){
        clearInterval(gl);
        console.clear();
        console.log("Game over");
        console.log("⭐ ", score);
        process.exit();
    }
}

function incScore(){
    score++;
    if(isFibonacciNumber(score)){
        life++;
        timeslot -= 5;
    }
}

readline.emitKeypressEvents(process.stdin);
process.stdin.on('keypress', (ch, key) => {
    if (key && key.ctrl && key.name == 'c') {
        process.exit();
    }
    if (key && key.name == 'space') {
        if (x == dig.x && y == dig.y){
            clearInterval(timerInt);

            incScore();
            placeIb();
        } else {
            decLife();
        }
    }
    if (key && key.name == 'right') {
        dig.x += 1;
        if (dig.x == dim.x){
            dig.x--;
        }
    }
    if (key && key.name == 'left') {
        dig.x -= 1;
        if (dig.x == -1){
            dig.x = 0;
        }
    }
    if (key && key.name == 'down') {
        dig.y += 1;
        if (dig.y == dim.y){
            dig.y--;
        }
    }
    if (key && key.name == 'up') {
        dig.y -= 1;
        if (dig.y == -1){
            dig.y = 0;
        }
    }
});
process.stdin.setRawMode(true);

const gl = setInterval(() => {
    let bane = [...new Array(dim.y)].map(() => [...new Array(dim.x)].map(() => '🤓'));
    bane[y][x] = '😄';
    bane[dig.y][dig.x] = '😎';
    console.clear();
    console.log("⭐ ", score, "     🍿 ", life, "     ⏱ ", time);
    console.table(bane);
}, 500);

function isPerfectSquare(n){
    if(Number.isInteger(Math.sqrt(n))){
        return true;
    } else {
        return false;
    }
}

function isFibonacciNumber(n){
    return isPerfectSquare(5*n*n + 4) || isPerfectSquare(5*n*n - 4);
}
